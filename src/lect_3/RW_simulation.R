#### parameterek beallitasa ####

sigma <- 5
TT <- 100
N <- 10000
X_TT <- rep(0, N)

#### idosor generalas ####

for (i in 1:N) {
    eps <- rnorm(n = TT, mean = 0, sd = sigma)
    X_TT[i] <- cumsum(eps)[TT]
}

#### abrazolas ####

plot(density(X_TT), main = "")

EX <- 0
DX <- sigma * sqrt(TT)

D <- round(DX)
x <- -200:200
y <- dnorm(x, mean = 0, sd = sigma * sqrt(TT))

lines(x, y, col = "red")

#### momentumok ####

message("Szimulált momentumok:")
message(" - varhato ertek: ", mean(X_TT))
message(" - szoras: ", sd(X_TT))
message("Elméleti momentumok:")
message(" - varhato ertek: ", EX)
message(" - szoras: ", DX)
