##### real-life no 2 ####

rates_raw <- read.table(file = "data/USD_GBP.txt", header = T)
rates_raw$date <- as.Date(paste0(rates_raw$year, "-", rates_raw$mon, "-", rates_raw$day))

usd_gbp <- data.frame(
    date = rates_raw$date,
    rate = rates_raw$rate
)

plot(rate ~ date, data = usd_gbp, type = "l")

print(adf.test(usd_gbp$rate))

usd_gbp$log_ret <- c(NA, diff(log(usd_gbp$rate)))

plot(log_ret ~ date, data = usd_gbp, type = "l")

print(adf.test(usd_gbp$log_ret[-1]))

pacf(usd_gbp$log_ret[-1])

# jobb ha mi csinaljuk meg a logdiff trafot, mert igy hozamokhoz
# jutunk mintha rabizzuk az algoritmusra aztan arfolyam kulonb-
# seget kepez, ami meg annyira nem intuitiv
auto.arima(usd_gbp$log_ret, d = 0, allowmean = T)
