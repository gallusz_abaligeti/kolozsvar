library(tidyverse)
library(tidyquant)

# oszlopok jelentese!!
snp <- tq_get(x = "GSPC", from = "2010-01-01", to = "2015-01-01")

View(snp)

snp %>% 
    filter(year(date) == 2012, month(date) == 2) %>% 
    ggplot(aes(x = date, close = close, open = open, high = high, adjusted = adjusted, low = low)) + geom_barchart()

# loghozam szamolasa

snp_close_return <- diff(log(snp$close))

snp <- snp %>% tq_mutate(
    select = close,
    mutate_fun = dailyReturn,
    type = "log",
    col_rename = "cls_returns"
)

summary(snp)

gghistogram(snp$cls_returns)
