#### Anscombe ujra ####
# Feladat! Futtasson linearis regressziot a megfelelo x-y parok kozott. Mit tapasztal?
# Abrazolja a regresszios maradekokat az x-ek fuggvenyeben! Mit tapasztal?

#### Huber (1981) pelda leverage ####
huber <- read.table("data/huber.txt", header = TRUE)

# Konyv peldaja kiegeszitve hianyzo ertekkel
huber$YNA <- huber$YBad
huber$YNA[huber$YNA == 0] <- NA

# Feladat! Abrazolja a kulonbozo y-okat x fuggvenyeben!
plot(YBad ~ x, data = huber, ylim = c(-12, 3))
plot(YGood ~ x, data = huber, ylim = c(-12,3))
plot(YNA ~ x, data = huber, ylim = c(-12,3))

# Regresszios eredmenyek
mBad <- lm(YBad ~ x, data = huber)
summary(mBad)
abline(mBad, col = "red")
mGood <- lm(YGood ~ x, data = huber)
summary(mGood)
abline(mGood, col = "green")
mNA <- lm(YNA ~ x, data = huber) # Hianyzo ertekekrol lasd na.action argumentum
summary(mNA)
abline(mNA)

# Regresszios diagnosztika (hat matrix)
lm.influence(mBad)$hat
hatvalues(mBad)
hatvalues(mGood)

# Kvadratikus modell (javaslat mas modellre)
mKvadr <- lm(YBad ~ x + I(x^2), data = huber)
summary(mKvadr)
xx <- data.frame(x = seq(-4, 10, by = 0.1))
yy <- predict(mKvadr, xx)

plot(xx$x, yy, ylim = c(-3, 3), type = "l", ylab = "y", xlab = "x", col = "red")
points(YBad ~ x, data = huber)
hatvalues(mKvadr)

# Reziduumok vizsgálata
mBad$residuals
mGood$residuals

# Standardizalt reziduumok
mBad$residuals/(1.55 * sqrt((1 - hatvalues(mBad)))) # sqrt(deviance(mBad)/df.residual(mBad))
rstandard(mBad)
rstandard(mGood)

# Cook's Distance
cooks.distance(mBad)
cooks.distance(mGood)

# Az lm ojektumra meghívott plot parancs a szokasosan hasznalt diagnosztikai eszkozoket abrazolja
par(mfrow = c(2, 2))
plot(mBad)
plot(mGood)
par(mfrow = c(1, 1))
<<<<<<< HEAD

=======
>>>>>>> 5214903ec68323433058be6dc008d49c0c9fc323

#### Takaritok ####
cleaning <- read.table("data/cleaning.txt", header = TRUE)

plot(Rooms ~ Crews, data = cleaning, xlab = "Number of Crews", ylab = "Number of Rooms Cleaned")
m4 <- lm(Rooms ~ Crews, data = cleaning)
abline(m4, col = "red", lwd = 2)
summary(m4)
predict(m4,
        newdata = 
            data.frame(Crews=c(4,16)),
        interval = "prediction",
        level=0.95)

# Diagnosztika abrak
StanRes <- rstandard(m4)
plot(cleaning$Crews, StanRes, xlab = "Number of Crews", ylab = "Standardized Residuals")

# Figure 3.17 on page 74
sabs <- sqrt(abs(StanRes))
plot(cleaning$Crews, sabs, xlab = "Number of Crews", ylab = "Square Root(|Standardized Residuals|)")
 
#Figure 3.18 on page 75
par(mfrow=c(2,2))
plot(m4)
par(mfrow=c(1,1))

# Regression output on page 77
sqrtcrews <- sqrt(cleaning$Crews)
sqrtrooms <- sqrt(cleaning$Rooms)
m5 <- lm(sqrtrooms~sqrtcrews)
summary(m5)
predict(m5, 
        newdata = data.frame(sqrtcrews = c(2,4)),
        interval="prediction", level = 0.95)

#Figure 3.20 on page 78
par(mfrow=c(1,2))
plot(sqrtcrews, sqrtrooms, xlab = "Square Root(Number of Crews)", ylab = "Square Root(Number of Rooms Cleaned)")
abline(m5, col = "red")

StanRes2 <- rstandard(m5)
plot(sqrtcrews, StanRes2, xlab = "Square Root(Number of Crews)", ylab = "Standardized Residuals")
par(mfrow=c(1,1))
