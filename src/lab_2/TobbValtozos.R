defects <- read.table("data/defects.txt", header=TRUE,  row.names = 1)

# Paronkenti x-y diagramok
pairs(defects)
pairs(Defective ~ Temperature + Density + Rate, data = defects)

library(GGally)
ggpairs(defects)

# A modell illesztese a ketvaltozoshoz nagyon hasonlo
mm1 <- lm(Defective ~ Temperature + Density + Rate,  data = defects)

summary(mm1)

# A konyv altal ajanlott diagnosztikai abrak
StanRes1 <- rstandard(mm1)
plot(defects$Temperature, StanRes1, ylab = "Standardized Residuals")
plot(defects$Density, StanRes1, ylab = "Standardized Residuals")
plot(defects$Rate, StanRes1, ylab = "Standardized Residuals")
plot(mm1$fitted.values, StanRes1, ylab = "Standardized Residuals", xlab = "Fitted Values")

# Box-Cox transzformacio
library(MASS)
boxcox(mm1)

# Javaslat: gyokvonas az eredmenyvaltozora
# Ovatosan, felulirja onmagat a valtozo
defects <- defects %>% mutate(Defective = sqrt(Defective))

ggpairs(defects) # sokkal linearisabb kapcsolat

mm2 <- lm(Defective ~ Temperature + Density + Rate,  data = defects)

summary(mm2)

# AIC kiszamitasa
extractAIC(mm2, k = 2)
#Calculate BIC
extractAIC(mm2, k = log(dim(defects)[1]))

mm3 <- lm(Defective ~ Temperature + Density,  data = defects)

# AIC kiszamitasa
extractAIC(mm3, k = 2)
#Calculate BIC
extractAIC(mm3, k = log(dim(defects)[1]))

# Multikollinearitas vizsgalata
library(car)
vif(mm2) # hüvelykujj szabalykent 5 feletti ertek eros multikollinearitast jelez...